#!/bin/sh

set -eu

if [ -n "${SCRIPT:-}" ]; then
	echo "$SCRIPT" >/dev/shm/script.sh
	sh /dev/shm/script.sh "$@"
else
	for sh in /scripts/*.sh; do
		if [ -r "$sh" ]; then
			sh "$sh"
		fi
	done
fi
