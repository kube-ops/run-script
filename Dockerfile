FROM alpine:3.14

# hadolint ignore=DL3017,DL3018,DL3019
RUN set -eux \
  ; apk update \
  ; apk upgrade \
  ; apk add bash ca-certificates curl jq \
  ; rm -f /var/cache/apk/* /var/lib/apk/* \
  ; addgroup -g 1000 k8s \
  ; adduser -h /home/k8s -g "Kubernetes Application" -s /bin/ash -G k8s -u 1000 -D k8s \
  ; bash --version \
  ; curl --version \
  ; jq --version \
  ; mkdir -p /scripts \
  ; rm -f ~/.ash_history

COPY --chown=root:root entrypoint.sh /usr/bin/entrypoint.sh
USER 1000:1000
VOLUME ["/scripts"]
ENTRYPOINT ["entrypoint.sh"]
